package com.example.dara.clientapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.android.kakak.clientapp.R;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectLocationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationChangeListener, GoogleMap.OnCameraChangeListener{

    private GoogleMap mMap;
    Location location;
    TextView lbl_current_location;
    String coordinateString;
    Button btn_select_location;

    static public String KEY_COOR = "KEY_COOR";
    static public String COOR_STRING = "COOR_STRING";
    static public String VAR_ADDRESS = "VAR_ADDRESS";
    ProgressDialog mDialog;

    static String LOCATING = "Locating...";

    boolean use_search;
    boolean has_located_my_location;

    String address;
    String TAG = "101";

    Place searched_place;
    String coor_string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_location);



        // LAYOUT
        btn_select_location = (Button)findViewById(R.id.btn_select_location);

        // SETUP
        setTitle("Select Delivery Location");

        // ADD PERMISSION
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // TODO: Consider calling
            ActivityCompat.requestPermissions(SelectLocationActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
            // ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            // public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.e("access", "gps access required");
            return;
        }

        // SET HUD
        mDialog = new ProgressDialog(this);
        mDialog.setMessage(LOCATING);
        mDialog.setCancelable(false);
        mDialog.show();
        mDialog.setCancelable(true);

        // INITIALIZATION
        use_search = false;

        // MAP LAYOUT
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // UI
        lbl_current_location = (TextView)findViewById(R.id.lbl_current_location);

        // GET FROM INTENT
        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            coor_string = null;
        } else {
            coor_string = extras.getString(COOR_STRING,"");
            mDialog.hide();
        }

        // ADD AUTO COMPLETE


        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS)
                .build();



        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setFilter(typeFilter);


        // WHEN AUTOCOMPLETE FOUND PLACE
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                searched_place = place;

                CameraUpdate center =
                        CameraUpdateFactory.newLatLng(place.getLatLng());
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(20);

                mMap.moveCamera(center);
                mMap.animateCamera(zoom);

                use_search = true;

                mMap.getUiSettings().setAllGesturesEnabled(false);
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }

        });



    }

    public void enablePanning(View v){
        mMap.getUiSettings().setAllGesturesEnabled(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // ZOOM TO ASTRA OFFICE IN SHAH ALAM
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(3.178396, 101.530944), 14.0f) );

        mMap.setMyLocationEnabled(true);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {

            googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        } else {
            Toast.makeText(this, "Location Error" , Toast.LENGTH_LONG).show();
        }

        if (coor_string != null){

            List<String> list = new ArrayList<String>(Arrays.asList(coor_string.split(",")));

            LatLng latLng = new LatLng(Double.parseDouble(list.get(0)), Double.parseDouble(list.get(1)));
            Log.i("Load from coo_string" , "string " + Double.parseDouble(list.get(0)) );
            CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
            mMap.moveCamera(center);

            has_located_my_location = true;
            mDialog.hide();
            //lbl_current_location.setText(coor_string);

        }else{

        }

        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        mMap.setOnMyLocationChangeListener(this);
        mMap.setOnCameraChangeListener(this);

    }

    @Override
    public void onMyLocationChange(Location myLocation) {

        Log.i("onMyLocationChange","asd");

        LatLng latLng;

        if (has_located_my_location == false){

            mDialog.hide();

            location = mMap.getMyLocation();
            latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            coordinateString = latLng.latitude + "," + latLng.longitude;

            CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
            mMap.moveCamera(center);
            has_located_my_location = true;
            coordinateString = latLng.latitude + "," + latLng.longitude;
            lbl_current_location.setText(coordinateString);

        }

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

        Log.i("onCameraChange","--");

        coordinateString = cameraPosition.target.latitude + "," + cameraPosition.target.longitude;
        //lbl_current_location.setText(coordinateString);
        lbl_current_location.setText(LOCATING);

        new ReverseGeocodingTask(getBaseContext()).execute(cameraPosition.target);

    }

//  @Override
//  public void onLocationChanged(Location location)
//  {
//    Log.i("location changed","");
//  }

    public void confirmLocation(View view){



        Log.i("ad", lbl_current_location.getText().toString() + " " +
                lbl_current_location.getText().toString());

        if (lbl_current_location.getText().toString().equals(LOCATING) ||
                lbl_current_location.getText().toString().equals("")){

            Toast.makeText(getApplicationContext(), "Please wait while we are finding the address.", Toast.LENGTH_SHORT).show();
            return;

        }

        Intent intent=new Intent();
        intent.putExtra(SelectLocationActivity.KEY_COOR,coordinateString);
        intent.putExtra(SelectLocationActivity.VAR_ADDRESS,address);
        setResult(MainActivity.LOCATION_REQUEST,intent);


        finish();//finishing activity

    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent();
        intent.putExtra(SelectLocationActivity.KEY_COOR,coordinateString);
        intent.putExtra(SelectLocationActivity.VAR_ADDRESS,address);
        setResult(MainActivity.LOCATION_REQUEST,intent);
        finish();//finishing activity

    }

    // REVERSE GEOCODING

    private class ReverseGeocodingTask extends AsyncTask<LatLng, Void, String> {
        Context mContext;

        public ReverseGeocodingTask(Context context){
            super();
            mContext = context;
        }

        // Finding address using reverse geocoding
        @Override
        protected String doInBackground(LatLng... params) {
            Geocoder geocoder = new Geocoder(mContext);
            double latitude = params[0].latitude;
            double longitude = params[0].longitude;

            List<Address> addresses = null;
            String addressText="";

            try {
                addresses = geocoder.getFromLocation(latitude, longitude,1);
            } catch (IOException e) {
                e.printStackTrace();

                String latlong = latitude + "," + longitude;
                // test another alternative
                getReverseGeocode(latlong);

            }

            if(addresses != null && addresses.size() > 0 ){
                Address address = addresses.get(0);

                addressText = String.format("%s, %s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        address.getLocality(),
                        address.getCountryName());
            }

            return addressText;
        }

        @Override
        protected void onPostExecute(String addressText) {
            // Setting the title for the marker.
            // This will be displayed on taping the marker
            address = addressText;
            lbl_current_location.setText(addressText);
            Log.i("ReverseGeocodingTask onPostExecute", address);

        }


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case 1:{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);

                } else {

                    finish();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    // EXTRA SUPPORT

    private void getReverseGeocode(String current_coordinate_string) {

        String url = "https://maps.google.com/maps/api/geocode/json?latlng=" + current_coordinate_string + "&key=AIzaSyC9PFMLvRuW5iFDWpWtG3m_NFN0o4Zb2X4";

        Log.i("url", url);


        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("onResponse", response);

                        JSONObject location;

                        try {
                            JSONObject obj = new JSONObject(response);

                            location = obj.getJSONArray("results").getJSONObject(0);

                            String location_string = location.getString("formatted_address");

                            Log.d("location", location_string);


                            lbl_current_location.setText(location_string);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", "error");
                    }
                }

        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();

                Log.i("sending ", params.toString());

                return params;
            }
        };


        RequestQueue sQueue = Volley.newRequestQueue(this);
        sQueue.add(postRequest);

    }

}
