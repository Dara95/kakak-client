package com.example.dara.clientapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.kakak.clientapp.RegistrationActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {


    @BindView(com.android.kakak.clientapp.R.id.editTextClientEmail)
    EditText editTextClientEmail;
    @BindView(com.android.kakak.clientapp.R.id.editTextClientPass)
    EditText editTextClientPass;
    @BindView(com.android.kakak.clientapp.R.id.buttonClientLogin)
    Button buttonClientLogin;
    @BindView(com.android.kakak.clientapp.R.id.buttonRegisterClient)
    Button buttonRegisterClient;

    private DatabaseReference mSearchedLocationReference;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.android.kakak.clientapp.R.layout.activity_login);
        ButterKnife.bind(this);
        loginButton();
        registrationButton();
    }


    public void loginButton(){
        buttonClientLogin.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                auth = FirebaseAuth.getInstance();
                final String Password = editTextClientPass.getText().toString();
                String Email = editTextClientEmail.getText().toString();
                auth.signInWithEmailAndPassword(Email, Password)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.

                                if (!task.isSuccessful()) {
                                    // there was an error
                                    if ( editTextClientPass.length() < 6) {
                                        editTextClientPass.setError("Password should contain more than 6 characters");
                                    } else {
                                        Toast.makeText(LoginActivity.this, "Authentication failed", Toast.LENGTH_LONG).show();
                                    }
                                } else {

                                    mSearchedLocationReference = FirebaseDatabase
                                            .getInstance()
                                            .getReference()
                                            .child("Clients");

                                    mSearchedLocationReference.addValueEventListener(new ValueEventListener() {

                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            // for (DataSnapshot emailSnapshot : dataSnapshot.getChildren()) {
                                            //   String email = emailSnapshot.child("email").getValue().toString();
                                            //    Log.d("Locations updated", "location: " + email);
                                            //   System.out.println(email);
                                            //    if (Email.getText().toString().equals(email)){
                                            Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_LONG).show();

                                            Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
                                            startActivity(intent);
                                            //  }
                                            //else {
                                            //  Intent intent2 = new Intent(Client.this, CleanerLoginActivity.class);
                                            //startActivity(intent2);
                                            //  }
                                            // }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            Toast.makeText(LoginActivity.this, "Error "+databaseError, Toast.LENGTH_LONG).show();

                                        }
                                    });


                                    //    finish();
                                }
                            }
                        });

            }
        });

    }

    public void registrationButton(){
        buttonRegisterClient.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
            }
        });
    }
}
