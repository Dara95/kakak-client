package com.example.dara.clientapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.kakak.clientapp.ClientArrayList;
import com.android.kakak.clientapp.Message;
import com.android.kakak.clientapp.OrderDetails;
import com.android.kakak.clientapp.R;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import io.fabric.sdk.android.Fabric;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {



    @BindView(R.id.btn_select_location)
    Button btnSelectLocation;
    @BindView(R.id.btn_date)
    Button btnDate;
    @BindView(R.id.btn_time)
    Button btnTime;
    @BindView(R.id.editText_notes)
    EditText editTextNotes;
    @BindView(R.id.btn_request)
    Button btnRequest;

    ImageView profile_pic;
    TextView username;
    TextView user_email;
    DatePickerDialog datePickerDialog;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    DatabaseReference myRef;
    static final int LOCATION_REQUEST = 101;

    //arrayList of orders
    OrderDetails ordersArray = new OrderDetails();
    String dataTitle, dataMessage;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main_menu);
        subscribeToTopic();
        //Set image, username and email to sidebar
        String id = "";

        LayoutInflater inflater = LayoutInflater.from(this);
        LinearLayout ll = (LinearLayout) findViewById(R.id.linear_layout_sidebar);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        username = (TextView) header.findViewById(R.id.username);
    /*    if (user.getUid() != null) {
            id = user.getUid();
            myRef = database.getReference("Clients").child(id);
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        ClientArrayList value = new ClientArrayList();
                        value.setEmail(snapshot.child("email").toString());
                        value.setImageUrl(snapshot.child("imageUrl").toString());
                        System.out.println("Snapshot " + snapshot);
                        Bitmap bitmap = null;
                        username.setText(value.getEmail());
                        System.out.println("Succesfully printed email: " + value.getEmail());
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(value.getImageUrl()));
                            //    profile_pic.setImageBitmap(bitmap);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }*/

        // Handle possible data accompanying notification message.
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                if (key.equals("title")) {
                    dataTitle=(String)getIntent().getExtras().get(key);
                }
                if (key.equals("message")) {
                    dataMessage = (String)getIntent().getExtras().get(key);;
                }
            }
            showAlertDialog();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        System.out.println("FCM Key "+ FirebaseInstanceId.getInstance().getToken());


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        ButterKnife.bind(this);
        datePicker();
        timePicker();
        locationKeeper();
        requestButtonPressed();
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Message");
        builder.setMessage("title: " + dataTitle + "\n" + "message: " + dataMessage);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    public void subscribeToTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic("notifications");
        Toast.makeText(this, "Subscribed to Topic: Notifications", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            startActivity(new Intent(MainMenuActivity.this, AboutKakakActivity.class));
            // Handle the camera action
        } else if (id == R.id.nav_history) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void locationKeeper(){
        // ADD PERMISSION
        btnSelectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this, SelectLocationActivity.class));
            }
        });

    }

    public void datePicker(){
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(MainMenuActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                btnDate.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

    }
    public void timePicker() {
        btnTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(MainMenuActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        btnTime.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == LOCATION_REQUEST) {
            String key_coor = "";

            try {
                key_coor = data.getStringExtra(SelectLocationActivity.KEY_COOR);
            } catch (NullPointerException npe) {
                key_coor = "";
            }

            String my_address = "";
            try {
                my_address = data.getStringExtra(SelectLocationActivity.VAR_ADDRESS);
            } catch (NullPointerException npe) {

            }


            String address = my_address;

            //mainFragment.bookBatteryFragment.btn_select_location.setText(message);
            if (address != null) {
                System.out.println("The address if not null "+ address);
                if (address.equals("")) {
                    System.out.println("The address if null "+ address);

                    address = "SELECT LOCATION";
                }
                btnSelectLocation.setText(address);

            }

            btnSelectLocation.setText(address);


        }
    }
    public void requestButtonPressed(){
        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clicked");
                ordersArray.setDate(btnDate.getText().toString());
                ordersArray.setTime(btnTime.getText().toString());
                ordersArray.setLatitude("3.4323");
                ordersArray.setLongitude("3.23123");
                ordersArray.setNotes(editTextNotes.getText().toString());
                Map<String, Object> childUpdates = new HashMap<>();
                final DatabaseReference myRef = database.getReference("Orders");
                final DatabaseReference myRef1 = database.getReference("messages");
                String key =  myRef.push().getKey();
                System.out.println("Key is "+key);

                childUpdates.put(key, ordersArray.toFirebaseObject());
                database.getReference("Orders").updateChildren(childUpdates, new DatabaseReference.CompletionListener() {

                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError == null) {
                            Log.d("Error", "error");
                            System.out.println("error"+databaseError);
                        }
                        Log.d("Success", "success");
                        System.out.println("success");
                        Toast.makeText(MainMenuActivity.this, "You have successfully requested the cleaner. You will receive push-notification once your order is accepted.",
                                Toast.LENGTH_SHORT).show();
                        myRef1.push().setValue(new Message("New order available.",  ""));
                        Toast.makeText(MainMenuActivity.this, "Message Sent", Toast.LENGTH_SHORT).show();

                    }
                });


            }
        });

    }
    private void sendNotification() {
        final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json=new JSONObject();
                    JSONObject dataJson=new JSONObject();
                    dataJson.put("body","Hi this is sent from device to device");
                    dataJson.put("title","dummy title");
                    json.put("notification",dataJson);
                    json.put("to", "/topics/news");
                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization","key="+"AIzaSyDg_wSQD63Ey1VzAV2cq6cna3H9MCqaXhM")
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                }catch (Exception e){
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }
}
