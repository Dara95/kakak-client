package com.android.kakak.clientapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegistrationActivity extends AppCompatActivity {

    @BindView(R.id.imageView_register_client)
    ImageView imageViewRegisterClient;
    @BindView(R.id.button_upload_image_registration)
    Button buttonUploadImageRegistration;
    @BindView(R.id.button_take_picture_registration)
    Button buttonTakePictureRegistration;
    @BindView(R.id.editText_client_username)
    EditText editTextClientUsername;
    @BindView(R.id.editText_client_pass_register)
    EditText editTextClientPassRegister;
    @BindView(R.id.editText_client_email_register)
    EditText editTextClientEmailRegister;
    @BindView(R.id.editText_phone_client_register)
    EditText editTextPhoneClientRegister;
    @BindView(R.id.button_register_client)
    Button buttonRegisterClient;



    //Firebase databse and authentication
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth= FirebaseAuth.getInstance();

    //Upload and Take Picture
    private static final int REQUEST_IMAGE_CAPTURE = 111;
    private static final int PICK_IMAGE_REQUEST = 234;
    String imageEncoded = null;


    //arrayList of cleaners
    ClientArrayList cleanerArray = new ClientArrayList();

    private Uri filePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        uploadImageButton();
        launchCameraButton();
        registerButtonClicked();
    }

    public void registerButtonClicked() {

        buttonRegisterClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Toast.makeText(RegistrationActivity.this, "Button clicked.",
                        Toast.LENGTH_SHORT).show();
                cleanerArray.setName(editTextClientUsername.getText().toString());
                cleanerArray.setPass(editTextClientPassRegister.getText().toString());
                cleanerArray.setEmail(editTextClientEmailRegister.getText().toString());
                cleanerArray.setPhoneNum(editTextPhoneClientRegister.getText().toString());
                cleanerArray.setImageUrl(imageEncoded);
                cleanerArray.setLatitude("0");
                cleanerArray.setLongtitude("0");

                mAuth.createUserWithEmailAndPassword(editTextClientEmailRegister.getText().toString(), editTextClientPassRegister.getText().toString())
                        .addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Toast.makeText(RegistrationActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();

                                if (!task.isSuccessful()) {
                                    Toast.makeText(RegistrationActivity.this, "Authentication failed." + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                } else {

                                    Toast.makeText(RegistrationActivity.this, "Success" + task.getException(),
                                            Toast.LENGTH_SHORT).show();


                                    String user = FirebaseAuth.getInstance().getCurrentUser().getUid();
                                    System.out.println(user);


                                    Map<String, Object> childUpdates = new HashMap<>();
                                    childUpdates.put(user, cleanerArray.toFirebaseObject());
                                    database.getReference("Clients").updateChildren(childUpdates, new DatabaseReference.CompletionListener() {

                                        @Override
                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                            if (databaseError == null) {
                                                // finish();
                                                Toast.makeText(RegistrationActivity.this, "Registered user", Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                Toast.makeText(RegistrationActivity.this, "Error " + databaseError, Toast.LENGTH_SHORT).show();

                                            }
                                        }
                                    });


                                }

                            }
                        });

            }
        });


    }

    public void uploadImageButton(){
        buttonUploadImageRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
    }

    public void launchCameraButton(){
        buttonTakePictureRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLaunchCamera();
            }
        });
    }



    public void encodeBitmapAndSaveToFirebase(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        imageEncoded = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
        DatabaseReference ref = FirebaseDatabase.getInstance()
                .getReference()
                .child("imageUrl");
        ref.setValue(imageEncoded);
    }

    //Launches Camera
    public void onLaunchCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageViewRegisterClient.setImageBitmap(bitmap);
                encodeBitmapAndSaveToFirebase(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //opens Photo Album
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
}
