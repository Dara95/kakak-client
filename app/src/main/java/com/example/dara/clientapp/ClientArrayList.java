package com.android.kakak.clientapp;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Dara on 03/07/2017.
 */

public class ClientArrayList implements Serializable {

    private String name;
    private String pass;
    private String email;
    private String phoneNum;
    private String notes;
    private String longtitude;
    private String latitude;
    private String imageUrl;
    public ClientArrayList(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public HashMap<String,String> toFirebaseObject() {
        HashMap<String,String> cleanerList =  new HashMap<String,String>();
        cleanerList.put("name", name);
        cleanerList.put("password", pass);
        cleanerList.put("email", email);
        cleanerList.put("phone number", phoneNum);
        cleanerList.put("notes", notes);
        cleanerList.put("latitude", latitude);
        cleanerList.put("longitude", longtitude);
        cleanerList.put("imageUrl", imageUrl);
        return cleanerList;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

