package com.example.dara.clientapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.android.kakak.clientapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutKakakActivity extends AppCompatActivity {

    @BindView(com.android.kakak.clientapp.R.id.close_button)
    Button closeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_kakak);
        ButterKnife.bind(this);
        closeButtonPressed();
    }
    public void closeButtonPressed(){
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutKakakActivity.this, MainMenuActivity.class));

            }
        });
    }
}
