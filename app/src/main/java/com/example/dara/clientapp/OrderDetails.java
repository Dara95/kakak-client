package com.android.kakak.clientapp;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Dara on 12/07/2017.
 */

public class OrderDetails implements Serializable {

    private String longitude;
    private String latitude;
    private String time;
    private String notes;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String date;

    public OrderDetails(){

    }

    public HashMap<String,String> toFirebaseObject() {
        HashMap<String,String> orderList =  new HashMap<String,String>();
        orderList.put("longitude", longitude);
        orderList.put("latitude", latitude);
        orderList.put("date", date);
        orderList.put("time", time);
        orderList.put("notes", notes);
        return orderList;
    }
}
